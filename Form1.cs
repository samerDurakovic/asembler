﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Asembler
{
    public partial class Form1 : Form
    {
        private Dictionary<string, string> simbols;
        private Dictionary<string, string> comp;
        private Dictionary<string, string> dest;
        private Dictionary<string, string> jmp;
        private Dictionary<string, string> labele;
        private static int BROJ;
        private static int PC;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonUcitaj_Click(object sender, EventArgs e)
        {
            simbols = new Dictionary<string, string>
            {
            { "R0", "0000000000000000" },
            { "R1", "0000000000000001" },
            { "R2", "0000000000000010" },
            { "R3", "0000000000000011" },
            { "R4", "0000000000000100" },
            { "R5", "0000000000000101" },
            { "R6", "0000000000000110" },
            { "R7", "0000000000000111" },
            { "R8", "0000000000001000" },
            { "R9", "0000000000001001" },
            { "R10", "0000000000001010" },
            { "R11", "0000000000001011" },
            { "R12", "0000000000001100" },
            { "R13", "0000000000001101" },
            { "R14", "0000000000001110" },
            { "R15", "0000000000001111" },
            { "SP", "0000000000000000" },
            { "LCL", "0000000000000001" },
            { "ARG", "0000000000000010" },
            { "THIS", "0000000000000011" },
            { "THAT", "0000000000000100" },
            { "SCREEN", "0100000000000000" },
            { "KBD", "0110000000000000" },
            };
            comp = new Dictionary<string, string>
            {
            { "0", "0101010" },
            { "1", "0111111" },
            { "-1", "0111010" },
            { "D", "0001100" },
            { "A", "0110000" },
            { "!D", "0001101" },
            { "!A", "0110001" },
            { "-D", "0001111" },
            { "-A", "0110011" },
            { "D+1", "0011111" },
            { "A+1", "0110111" },
            { "D-1", "0001110" },
            { "A-1", "0110010" },
            { "D+A", "0000010" },
            { "D-A", "0010011" },
            { "A-D", "0000111" },
            { "D&A", "0000000" },
            { "D|A", "0010101" },
            { "M", "1110000" },
            { "!M", "1110001" },
            { "-M", "1110011" },
            { "M+1", "1110111" },
            { "M-1", "1110010" },
            { "D+M", "1000010" },
            { "D-M", "1010011" },
            { "M-D", "1000111" },
            { "D&M", "1000000" },
            { "D|M", "1010101" },
            };
            dest = new Dictionary<string, string>
            {
            { "null", "000" },
            { "M", "001" },
            { "D", "010" },
            { "MD", "011" },
            { "A", "100" },
            { "AM", "101" },
            { "AD", "110" },
            { "AMD", "111" },
            };
            jmp = new Dictionary<string, string>
            {
            { "null", "000" },
            { "JGT", "001" },
            { "JEQ", "010" },
            { "JGE", "011" },
            { "JLT", "100" },
            { "JNE", "101" },
            { "JLE", "110" },
            { "JMP", "111" },
            };

            labele = new Dictionary<string, string>();

            BROJ = 16;
            PC = 0;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.Text = "";
                try
                {
                    var filePath = openFileDialog1.FileName;
                    using (StreamReader file = new StreamReader(filePath))
                    {
                        string ln;
                        while ((ln = file.ReadLine()) != null)
                        {
                            
                            richTextBox1.Text += ln.Trim();
                            richTextBox1.Text += "\n";
                        }
                        
                        file.Close();
                    }
                }
                catch
                {
                    MessageBox.Show("Greska");
                }

            }
        }

        private void buttonPrevedi_Click(object sender, EventArgs e)
        {
            string[] lista = richTextBox1.Text.Split('\n');
            List<string> lista2 = new List<string>();
            string result="";
            richTextBox2.Text = "";
            foreach (string s in lista)
            {
            if (!s.Equals(String.Empty))
            {
                    
                if (!(s[0] == '/' && s[1] == '/'))
                {

                        int brojac = s.IndexOf(' ');
                        if (brojac > 0)
                            result = s.Substring(0, brojac);
                        else
                            result = s;

                        lista2.Add(result);
                                
                        if (result[0] == '(' && result.EndsWith(")"))
                        {
                            string[] simbol = result.Split('(', ')');

                            if (!labele.ContainsKey(simbol[1]))
                                labele[simbol[1]] = pretrvoriUBinarno(PC);
                        }
                        else
                        {
                            PC++;
                        }
                }
            }

        }

            foreach (string r in lista2)
            {
                if (!r.Equals(String.Empty) && !(r[0] == '/' && r[1] == '/') && !(r[0] == '(' && r.EndsWith(")")))
                {
                    if (r[0] == '@')
                        instrukcijaA(r);
                    else
                        instrukcijaC(r);
                }
            }

        }

        private string pretrvoriUBinarno(int n)
        {
            List<int> niz = new List<int>();
            int i = 0;
            while (n > 0)
            {
                niz.Add(n % 2);
                n = n / 2;
                i++;
            }

            string s = "0";
            for (int j = 0; j < (15 - niz.Count); j++)
                s += "0";

            for (i = i - 1; i >= 0; i--)
                s += niz[i];

            return s;
        }

        private bool daLiJeBroj(string n)
        {
            for (int i = 0; i < n.Length; i++)
                if (!Char.IsNumber(n[i]))
                    return false;
            return true;
        }

        private void instrukcijaA(string s)
        {
            string[] simbol = s.Split('@');

            if (daLiJeBroj(simbol[1]))
            {
                richTextBox2.Text += pretrvoriUBinarno(Convert.ToInt32(simbol[1]));
                richTextBox2.Text += "\n";
            }
            else
            {
                if(labele.ContainsKey(simbol[1]))
                {
                    richTextBox2.Text += labele[simbol[1]];
                    richTextBox2.Text += "\n";
                }
                else if (simbols.ContainsKey(simbol[1]))
                {
                    richTextBox2.Text += simbols[simbol[1]];
                    richTextBox2.Text += "\n";
                }
                else
                {
                    simbols[simbol[1]] = pretrvoriUBinarno(BROJ);
                    BROJ++;

                    richTextBox2.Text += simbols[simbol[1]];
                    richTextBox2.Text += "\n";
                }
            }
        }

        private void instrukcijaC(string s)
        {
            string cInstrukcija = "111";

            if (!s.Contains(';'))
            {
            if (s.Contains('='))
            { 
            string[] simboli = s.Split('=');
            cInstrukcija += comp[simboli[1]] + dest[simboli[0]];
            }
            else
            {
            cInstrukcija += comp[s] + dest["null"];
            }
                cInstrukcija += jmp["null"];
            }

            else
            {
                string[] simboli = s.Split(';');
                cInstrukcija += comp[simboli[0]] + dest["null"] + jmp[simboli[1]];
            }
            richTextBox2.Text += cInstrukcija;
            richTextBox2.Text += "\n";
        }

        private void labela(string s)
        {
            string[] simbol = s.Split('(', ')');

            if (!labele.ContainsKey(simbol[1]))
                labele[simbol[1]] = pretrvoriUBinarno(PC);

            richTextBox2.Text += "\n";
        }

    }
}
